# GitHub Tools
[![Build Status](https://travis-ci.org/shibme/github-tools.svg)](https://travis-ci.org/shibme/github-tools)
[![Percentage of issues still open](http://isitmaintained.com/badge/open/shibme/github-tools.svg)](http://isitmaintained.com/project/shibme/github-tools "Percentage of issues still open")

A small bunch of tools to automate some tasks on GitHub