package me.shib.java.tools.github.tasks;

import java.io.IOException;

public class PermissionProviderLauncher {

    public static void main(String[] args) throws IOException, InterruptedException {
        PermissionProvider pp = new PermissionProvider(System.getenv("GITHUB_TOKEN"));
        pp.addAllReposToTeam("test", "security");
    }

}
