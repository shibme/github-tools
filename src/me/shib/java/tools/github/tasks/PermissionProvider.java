package me.shib.java.tools.github.tasks;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.eclipse.egit.github.core.Repository;
import org.eclipse.egit.github.core.Team;
import org.eclipse.egit.github.core.client.GitHubClient;
import org.eclipse.egit.github.core.service.RepositoryService;
import org.eclipse.egit.github.core.service.TeamService;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PermissionProvider {

    private transient Gson gson;
    private transient TeamService teamService;
    private transient Map<String, Team> teamMap;
    private transient RepositoryService repositoryService;

    public PermissionProvider(String githubAdminToken) {
        this.gson = new GsonBuilder().setPrettyPrinting().create();
        GitHubClient client = new GitHubClient();
        client.setOAuth2Token(githubAdminToken);
        this.teamService = new TeamService(client);
        this.teamMap = new HashMap<>();
        this.repositoryService = new RepositoryService(client);
    }

    private Team getTeamForName(String org, String teamName) throws IOException {
        Team teamToFind = teamMap.get(org + "/@" + teamName);
        if (teamToFind == null) {
            List<Team> teams = teamService.getTeams(org);
            for (Team team : teams) {
                if (team.getName().contentEquals(teamName)) {
                    teamToFind = teamService.getTeam(team.getId());
                    teamMap.put(org + "/@" + teamName, teamToFind);
                }
            }
        }
        return teamToFind;
    }

    private boolean isRepoInList(List<Repository> repos, Repository repo) {
        for (Repository r : repos) {
            if (repo.getName().equalsIgnoreCase(r.getName()) &&
                    repo.getOwner().getLogin().equalsIgnoreCase(r.getOwner().getLogin())) {
                return true;
            }
        }
        return false;
    }

    public void addAllReposToTeam(String org, String teamName) throws IOException {
        Team team = getTeamForName(org, teamName);
        List<Repository> orgRepos = repositoryService.getOrgRepositories(org);
        List<Repository> teamRepos = teamService.getRepositories(team.getId());
        System.out.println("Total Repos in org (" + org + "): " + orgRepos.size());
        System.out.println("Total Repos Already in Team (" + teamName + "): " + teamRepos.size());
        int added = 0;
        int skipped = 0;
        int count = 0;
        for (Repository repo : orgRepos) {
            count++;
            if (isRepoInList(teamRepos, repo)) {
                System.out.println(count + ". Repo (" + repo.getOwner().getLogin() + "/" + repo.getName() +
                        ") already exists");
                skipped++;
            } else {
                System.out.println(count + ". Repo (" + repo.getOwner().getLogin() + "/" + repo.getName() +
                        ") was added to " + team.getName());
                teamService.addRepository(team.getId(), repo);
                added++;
            }
        }
        System.out.println("Added " + added + " repos and skipped " + skipped + " repos.");
    }

}
