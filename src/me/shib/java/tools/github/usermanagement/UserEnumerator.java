package me.shib.java.tools.github.usermanagement;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.eclipse.egit.github.core.User;
import org.eclipse.egit.github.core.client.GitHubClient;
import org.eclipse.egit.github.core.service.OrganizationService;
import org.eclipse.egit.github.core.service.UserService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class UserEnumerator {

    public static void main(String[] args) throws IOException, InterruptedException {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        GitHubClient client = new GitHubClient();
        client.setOAuth2Token(System.getenv("GITHUB_TOKEN"));
        OrganizationService orgService = new OrganizationService(client);
        List<User> users = orgService.getMembers("orgname");
        SyncQueue syncQueue = new SyncQueue(new LinkedList<>(users));
        List<UserDetailCollector> collectors = new ArrayList<>();
        UserService userService = new UserService(client);
        for (int i = 0; i < 50; i++) {
            UserDetailCollector collector = new UserDetailCollector(userService, syncQueue);
            collectors.add(collector);
            collector.start();
        }
        for (UserDetailCollector collector : collectors) {
            collector.join();
        }
        for (User user : syncQueue.getProcessedUsers()) {
            System.out.println(user.getName() + "," + user.getLogin() + "," + user.getEmail());
        }
    }

}
