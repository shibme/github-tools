package me.shib.java.tools.github.usermanagement;

import org.eclipse.egit.github.core.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

public class SyncQueue {

    private Queue<User> userQueue;
    private List<User> users;

    public SyncQueue(Queue<User> userQueue) {
        this.userQueue = userQueue;
        this.users = new ArrayList<>();
    }

    public synchronized User poll() {
        return userQueue.poll();
    }

    public List<User> getProcessedUsers() {
        return users;
    }

    public synchronized void addWorkItemInfo(User user) {
        users.add(user);
    }

}
