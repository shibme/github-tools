package me.shib.java.tools.github.usermanagement;

import org.eclipse.egit.github.core.User;
import org.eclipse.egit.github.core.service.UserService;

import java.io.IOException;

public class UserDetailCollector extends Thread {

    private SyncQueue syncQueue;
    private UserService userService;

    public UserDetailCollector(UserService userService, SyncQueue syncQueue) {
        this.userService = userService;
        this.syncQueue = syncQueue;
    }

    @Override
    public void run() {
        User user;
        while ((user = syncQueue.poll()) != null) {
            try {
                syncQueue.addWorkItemInfo(userService.getUser(user.getLogin()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
