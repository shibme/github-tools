package me.shib.java.tools.github.auditor;

import me.shib.java.tools.github.auditor.models.UserBranches;
import org.eclipse.egit.github.core.Repository;
import org.eclipse.egit.github.core.RepositoryBranch;
import org.eclipse.egit.github.core.RepositoryCommit;
import org.eclipse.egit.github.core.client.GitHubClient;
import org.eclipse.egit.github.core.service.CommitService;
import org.eclipse.egit.github.core.service.RepositoryService;
import org.eclipse.egit.github.core.service.UserService;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class GitHubAuditor {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    private static final GitHubClient gitHubClient = new GitHubClient();

    private static void forkedRepos(GitHubClient gitHubClient) throws IOException {
        RepositoryService repositoryService = new RepositoryService(gitHubClient);
        Scanner scanner = new Scanner(System.in);
        List<Repository> repositories = null;
        String org = null;
        while (repositories == null) {
            System.out.println("\nProvide your org name:");
            org = scanner.next();
            try {
                repositories = repositoryService.getOrgRepositories(org);
            } catch (IOException e) {
                System.out.println("The org that was provided is invalid. Please try again.");
            }
        }
        System.out.println("Total repos: " + repositories.size() + "\n");
        int forked_from_org = 0;
        int totally_forked_to = 0;
        for (Repository repository : repositories) {
            if (repository.getForks() > 0) {
                List<Repository> forkedToRepos = repositoryService.getForks(repository);
                forked_from_org++;
                totally_forked_to += forkedToRepos.size();
                for (Repository forkedToRepo : forkedToRepos) {
                    System.out.println(repository.isPrivate() + "\t" + repository.getOwner().getLogin() + "/" + repository.getName() + "\t" + forkedToRepo.getOwner().getLogin() + "/" + forkedToRepo.getName());
                }
            }
        }
        System.out.println("\nRepos forked from " + org + ": " + forked_from_org);
        System.out.println("Total repos forked to: " + totally_forked_to);
    }

    private static void listLang(GitHubClient gitHubClient) {
        RepositoryService repositoryService = new RepositoryService(gitHubClient);
        Scanner scanner = new Scanner(System.in);
        String org = null;
        List<Repository> repositories = null;
        while (repositories == null) {
            System.out.println("\nProvide your org name:");
            org = scanner.next();
            try {
                repositories = repositoryService.getOrgRepositories(org);
            } catch (IOException e) {
                System.out.println("The org that was provided is invalid. Please try again.");
            }
        }
        Set<String> languages = new HashSet<>();
        for (Repository repository : repositories) {
            if (repository.isPrivate()) {
                languages.add(repository.getLanguage());
            }
        }
        System.out.println("Total Languages used across all repos in " + org + ": " + languages.size());
        for (String lang : languages) {
            System.out.println(lang);
        }
    }

    private static void branchCleanUp(GitHubClient gitHubClient) throws IOException {
        CommitService commitService = new CommitService(gitHubClient);
        Scanner scanner = new Scanner(System.in);
        RepositoryService service = new RepositoryService(gitHubClient);
        Repository repository = null;
        while (repository == null) {
            System.out.println("\nProvide the repository to scan (in username/repo format):");
            String fullRepoName = scanner.next();
            if (fullRepoName.split("/").length == 2) {
                try {
                    String[] splitOfFullRepoName = fullRepoName.split("/");
                    repository = service.getRepository(splitOfFullRepoName[0], splitOfFullRepoName[1]);
                } catch (IOException e) {
                    System.out.println("The repository that was provided does not exist. Please try again.");
                }
            } else {
                System.out.println("The repository that was provided is invalid. Please try again.");
            }
        }
        int days = 0;
        while (days < 1) {
            System.out.println("\nProvide the number of days past commit:");
            days = scanner.nextInt();
            if (days < 1) {
                System.out.println("The number of days past commit should be greater than 0.");
            }
        }
        long daysInMillis = days * 86400000L;
        long timeLimit = new Date().getTime() - daysInMillis;
        List<RepositoryBranch> branches = service.getBranches(repository);
        System.out.println("\nTotal number of branches: " + branches.size());
        System.out.println("Listing branches older than: " + new Date(timeLimit) + "\n");
        int outdated = 0;
        int processed = 0;
        Map<String, UserBranches> userBranchesMap = new HashMap<>();
        for (RepositoryBranch branch : branches) {
            RepositoryCommit repositoryCommit = commitService.getCommit(repository, branch.getCommit().getSha());
            Date date = repositoryCommit.getCommit().getAuthor().getDate();
            if (date.getTime() < timeLimit) {
                String username;
                if (repositoryCommit.getAuthor() != null) {
                    username = repositoryCommit.getAuthor().getLogin();
                } else {
                    username = "Username Unavailable";
                }
                String name = repositoryCommit.getCommit().getAuthor().getName();
                String email = repositoryCommit.getCommit().getAuthor().getEmail();
                UserBranches userBranch = userBranchesMap.get(username);
                if (userBranch == null) {
                    userBranch = new UserBranches(username);
                    userBranchesMap.put(username, userBranch);
                }
                userBranch.setName(name);
                userBranch.setEmail(email);
                userBranch.addBranch(branch.getName(), date);
                outdated++;
            }
            processed++;
            System.out.print("Processed branch: " + processed + "/" + branches.size() + "\r");
        }
        System.out.println("Number of outdated branches/total number of branches: " + outdated + "/" + branches.size());
        System.out.println("Branches older than " + new Date(timeLimit) + ":");
        Set<String> users = userBranchesMap.keySet();
        for (String user : users) {
            UserBranches userBranch = userBranchesMap.get(user);
            Set<UserBranches.UserBranchData> commitDataSet = userBranch.getUserBranchData();
            for (UserBranches.UserBranchData commitData : commitDataSet) {
                System.out.println(userBranch.getUsername() + "\t\t" + userBranch.getName() + "\t\t" + userBranch.getEmail()
                        + "\t\t" + dateFormat.format(commitData.getLastCommitDate()) + "\t\t" + commitData.getBranchName());
            }
        }
    }

    private static synchronized GitHubClient getGitHubClient(String access_token) {
        Scanner scanner = new Scanner(System.in);
        if (access_token == null) {
            System.out.println("Please provide your GitHub access token:");
            access_token = scanner.next();
        }
        UserService userService = new UserService(gitHubClient);
        boolean retry = true;
        while (retry) {
            gitHubClient.setOAuth2Token(access_token);
            try {
                userService.getUser();
                retry = false;
            } catch (IOException e) {
                System.out.println("\nYour access token is invalid. Please provide a valid access token:");
                access_token = scanner.next();
            }
        }
        return gitHubClient;
    }

    private static boolean actUpon(GitHubClient gitHubClient) {
        System.out.println("\nPlease select one of the below task to perform:");
        System.out.println("1. List forked private repos in the org");
        System.out.println("2. List languages used across the org");
        System.out.println("3. List branches to be purged");
        System.out.println("4. Exit\n");
        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();
        try {
            switch (choice) {
                case 1:
                    forkedRepos(gitHubClient);
                    break;
                case 2:
                    listLang(gitHubClient);
                    break;
                case 3:
                    branchCleanUp(gitHubClient);
                    break;
                case 4:
                    return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static void main(String[] args) {
        GitHubClient gitHubClient;
        if (args.length == 1) {
            gitHubClient = getGitHubClient(args[0]);
        } else {
            gitHubClient = getGitHubClient(null);
        }
        while (actUpon(gitHubClient)) {
        }
    }
}
