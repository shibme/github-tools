package me.shib.java.tools.github.auditor.models;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class UserBranches {

    private String username;
    private String name;
    private String email;
    private Set<UserBranchData> userBranchData;
    public UserBranches(String username) {
        this.username = username;
        this.userBranchData = new HashSet<>();
    }

    public void addBranch(String branchName, Date lastCommitDate) {
        this.userBranchData.add(new UserBranchData(branchName, lastCommitDate));
    }

    public Set<UserBranchData> getUserBranchData() {
        return userBranchData;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (this.name == null) {
            this.name = name;
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if (this.email == null) {
            this.email = email;
        }
    }

    public class UserBranchData {
        private String branchName;
        private Date lastCommitDate;

        private UserBranchData(String branchName, Date lastCommitDate) {
            this.branchName = branchName;
            this.lastCommitDate = lastCommitDate;
        }

        public String getBranchName() {
            return branchName;
        }

        public Date getLastCommitDate() {
            return lastCommitDate;
        }
    }
}
